@extends('layouts.site')

@section('content')

<div class="jumbotron">
			<div class="container">
				<h1>Hello Lara!</h1>
				<p>This is text, where is many information about this article</p>
				<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
			</div>
		</div>
		<div class="container">
			<div class="row">
				@foreach($articles as $article)
				<div class="col-md-4">
						<h2>{{ $article->title }}</h2>
						<p>{{ $article->description }}</p>
						<p><a class="btn btn-default" href="{{ route('articleShow',['id'=>$article->id]) }}" role="button">View details</a></p>
						<form action="{{ route('articleDelete',['article'=>$article->id]) }}" method="POST">
							<!-- <input type="hidden" name="_method" value="DELETE"> -->
							{{method_field('DELETE')}}
							{{csrf_field()}}
							<button type="submit" class="btn btn-danger">Delete</button>
						</form>
					</div>
				@endforeach
			</div>
			<hr>
			<footer>
				<p>&copy; 2018 Company, Ludok</p>
			</footer>
		</div>
@endsection