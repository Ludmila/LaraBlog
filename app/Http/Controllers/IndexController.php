<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class IndexController extends Controller
{
	protected $message;
	protected $header;

	public function __constract(){
		$this->header = 'Hello Lara!';
    	$this->message = 'This is text, where is many information about this article';
	}

    public function index(){
    	$article = Article::select(['id', 'title','description'])->get();
    	return view('page')->with(['message'=>$this->message, 
    								'header'=>$this->header,
    								'articles'=>$article
    							]);
    }

    public function show($id){
    	//$article = Article::find($id);
    	//WHERE id = $id
    	$article = Article::select(['id','title','text'])->where('id',$id)->first();
    	//dump($article);
    	return view('article-content')->with(['message'=>$this->message, 
    											'header'=>$this->header,
    											'article'=>$article
    										]);

    }

    public function add(){
    	return view('add-content')->with(['message'=>$this->message, 
    										'header'=>$this->header
    									]);
    }

    public function store(Request $request){
        $this->validate($request, [
            'title'=>'required|max:255',
            'alias'=>'required|unique:articles,alias',
            'text'=>'required'
        ]
        );

        $data = $request->all();
        $article = new Article;
        $article->fill($data);
        $article->save();

        return redirect('/');
    	//dump($article);
    }
}
