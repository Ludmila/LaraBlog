<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('hello', function() {
// 	return 'Hello world!!!';
// });

Route::get('/', 'IndexController@index');

//используя псевдоним можно создать ссылку на тот или другой маршрут
//псевдоним-name('articleShow')
Route::get('article/{id}', 'IndexController@show')->name('articleShow');

Route::get('page/add', 'IndexController@add');
Route::post('page/add', 'IndexController@store')->name('articleStore');

Route::delete('page/delete/{article}', function($article){
	$article_tmp = \App\Article::where('id',$article)->first();
	$article_tmp->delete();
	return redirect('/');
	//dump($article_tmp);
})->name('articleDelete');