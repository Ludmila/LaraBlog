@extends('layouts.site')

@section('content')

<div class="jumbotron">
	<div class="container">
		<h1>Hello Lara!</h1>
		<p>This is text, where is many information about this article</p>
		<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
	</div>
</div>
<div class="container">
	<div class="row">
		@if($article)
			<div>
				<h2>{{ $article->title }}</h2>
				<p>{{ $article->text }}</p>
			</div>
		@endif
	</div>
	<hr>
	<footer>
		<p>&copy; 2018 Company, Ludok</p>
	</footer>
</div>
@endsection